const mongoose = require('mongoose');

const connect = async () => {
    try {
        await mongoose.connect(process.env.DB_URL, {});
        console.log('Connection mongodb successful');  // Agrega esta línea
    } catch (err) {
        console.log(err);
        throw new Error('Connection mongodb error');
    }
}

module.exports = { connect };