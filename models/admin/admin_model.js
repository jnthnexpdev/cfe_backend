const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');
const schema = mongoose.Schema;

const admin_model = new schema({
    Nombre : {type: String, required : true},
    Email : {type: String, required : true},
    RPE : {type: String, required : true},
    Celular : {type: String},
    Password : {type: String, required : true},
    Fecha_Registro : {type: String},
    Hora_Registro : {type: String},
    Cuenta : {type: String, required : true},
    Departamento : {type : String, required : true},
});

admin_model.pre('save', function(next){
    const admin = this;

    if(!admin.isModified('Password')) return next();

    bcryptjs.genSalt(12, (err, salt) => {
        if(err) return next(err);

        bcryptjs.hash(admin.Password, salt, (err, hash) => {
            if(err) return next(err);

            admin.Password = hash;
            next();
        });
    });
});

module.exports = mongoose.model('Administradores', admin_model);