const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const post_model = new Schema({
    //Datos de la publicacion
    Id_Administrador : {type : mongoose.Schema.Types.ObjectId, ref : 'Administradores'},
    Id_Vehiculo: {type: mongoose.Schema.Types.ObjectId, ref: 'Vehiculos', required : true},
    Id_Reparacion : {type : mongoose.Schema.Types.ObjectId, ref : 'Publicaciones'},
    Contenido_Publicacion : {type : String},
    Fecha_Publicacion: {type: String},
    Hora_Publicacion : {type : String},
    Carpeta_Documentos: {type: String, required : true},
    Estatus : {type : String},
    Departamento : {type : String},
    //Datos del usuario que agrega un comentario
    Comentarios: [{
        Id_Usuario : {
            type : mongoose.Schema.Types.ObjectId,
            refPath : 'Comentarios.Tipo_Usuario',
        },
        Tipo_Usuario : {type : String},
        Contenido: {type : String},
        Hora : {type : String},
        Fecha : {type : String}
    }],
});

module.exports = mongoose.model('Publicaciones', post_model);