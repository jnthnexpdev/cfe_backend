const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');
const Schema = mongoose.Schema;

const workshop_model = new Schema ({
    Nombre : {type: String, required : true},
    Nombre_Responsable: {type: String, required : true},
    Celular: {type: String, required : true},
    Celular2: {type: String},
    Direccion: {type: String, required : true},
    Email : {type: String},
    Zona: {type:String, required : true},
    Centro_Trabajo: {type: String, required : true},
    Password : {type: String, required : true},
    Cuenta : {type: String}, //Tipo de cuenta Mecanico
    Departamento : {type : String},
    Fecha_Registro : {type: String},
    Hora_Registro : {type: String},
    Reparaciones : [{
        Id_Reparacion : {type : mongoose.Types.ObjectId, ref : 'Reparaciones'}
    }]
});

workshop_model.pre('save', function(next){
    const workshop = this;

    if(!workshop.isModified('Password')) return next();

    bcryptjs.genSalt(12, (err, salt) => {
        if(err) return next(err);

        bcryptjs.hash(workshop.Password, salt, (err, hash) => {
            if(err) return next(err);

            workshop.Password = hash;
            next();
        });
    });
});

module.exports = mongoose.model('Talleres', workshop_model);