const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');
const schema = mongoose.Schema;

const manager_model = new schema({
    Nombre : {type: String, required : true},
    Email : {type: String, required : true},
    RPE : {type: String, required : true},
    Celular : {type: String},
    Password : {type: String, required : true},
    Fecha_Registro : {type: String},
    Hora_Registro : {type: String},
    Cuenta : {type: String, required : true},
    Departamento : {type : String, required : true},
});

manager_model.pre('save', function(next){
    const manager = this;

    if(!manager.isModified('Password')) return next();

    bcryptjs.genSalt(12, (err, salt) => {
        if(err) return next(err);

        bcryptjs.hash(manager.Password, salt, (err, hash) => {
            if(err) return next(err);

            manager.Password = hash;
            next();
        });
    });
});

module.exports = mongoose.model('Jefes', manager_model);