const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const vehicle_model = new Schema({
    //Informacion control vehiculo
    Economico_Vehiculo : {type: String},

    //Informacion del responsable y departamento
    Telefono_Encargado : {type : String, default : 0},
    RPE_Resguardante : {type: String},
    Nombre_Resguardante : {type: String},
    Numero_Activo: {type: String},
    Division : {type: String},
    Zona: {type: String},
    Departamento_Vehiculo : {type: String},
    Area: {type: String},
    Centro_Trabajo: {type: String},

    //Informacion basica acerca del vehiculo
    Dias_Descanso : {type : String},
    Financiamiento_Vehiculo: {type: String},
    Placas_Vehiculo : {type: String},
    Traccion_Vehiculo: {type: String},
    Combustible_Vehiculo: {type: String},
    Uso_Vehiculo: {type: String},
    Estatus_Vehiculo: {type: String},
    Estatus_MYSAP_Vehiculo: {type: String},
    Ubicacion_Fisica_Vehiculo: {type: String},
    Verificacion_Vehiculo: {type: String},
    Problema_Verificacion_Vehiculo: {type: String},

    //Informacion acerca del chasis
    Marca_Chasis : {type: String},
    Submarca_Chasis : {type: String},
    Modelo_Chasis: {type: Number},
    Numero_Serie_Chasis : {type: String},
    Tipo_Chasis : {type: String},

    //Informacion acerca del hidraulico
    Marca_Hidraulico: {type: String},
    Tipo_Hidraulico : {type: String},
    Submarca_Hidraulico: {type: String},
    Modelo_Hidraulico: {type: Number},

    //Informacion acerca de las reparaciones del vehiculo
    Historial_Reparaciones_Vehiculo : [{
        Id_Reparacion: {type: mongoose.Schema.Types.ObjectId, ref: 'Reparaciones'},
    }],
    Cantidad_Reparaciones: {type: Number}

});

module.exports = mongoose.model('Vehiculos', vehicle_model);