const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const repair_model = new Schema({
    Id_Administrador : {type : mongoose.Schema.Types.ObjectId, ref : 'Administradores'},
    Id_Vehiculo: {type: mongoose.Schema.Types.ObjectId, ref: 'Vehiculos', required : true},
    Id_Mecanico: {type: mongoose.Schema.Types.ObjectId, ref: 'Mecanicos'},
    Diagnostico_Previo: {type: String, required : true},
    Estatus : {type : String},
    Dias_Descompuesto: { type: Number, default: 0 },
    Presupuesto_Inicial: {
        type: Number,
        min: 0,
        max: 1000000,
        get: value => value.toFixed(2),
        set: value => parseFloat(value.toFixed(2)),
        default : 0
    },
    Presupuesto_Final: {
        type: Number,
        min: 0,
        max: 1000000,
        get: value => value.toFixed(2),
        set: value => parseFloat(value.toFixed(2)),
        default : 0
    },
    //Fecha real cuando se descompuso el vehiculo
    Fecha_Problema: {type: String, default : null},
    //Fecha cuando se registra la reparacion
    Fecha: {type: String},
    //Fecha cuando se recibe en un taller mecanico
    Fecha_Taller: {type: String, default : null},
    //Fecha en la cual el taller entrega el vehiculo
    Fecha_Entrega: {type: String, default : null},
    Carpeta_Documentos: {type: String, required : true},
    Etapa_Reparacion : [{
        Titulo_Etapa : {type: String},
        Fecha_Etapa: {type: String},
        Hora_Etapa : {type : String},
        Descripcion_Etapa : {type: String},
        Carpeta_Documentos_Etapa: {type: String},
    }],
});

module.exports = mongoose.model('Reparaciones', repair_model);