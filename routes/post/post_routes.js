const express = require('express');
const cookie_parser = require('cookie-parser');
const {user_auth} = require('../../middlewares/auth_middleware');
const post_controll = require('../../controllers/post/post_controllers');
const router = express.Router();
const dotenv = require('dotenv');
dotenv.config();
const token_key = process.env.SECRET_KEY;

router.use(cookie_parser(token_key));


router.post('/api/v1/cfe/post/add-comment/:id', user_auth(), post_controll.addComment);
router.get('/api/v1/cfe/post/count', user_auth(), post_controll.countPosts);
router.get('/api/v1/cfe/post/all', user_auth(), post_controll.allPosts);
router.get('/api/v1/cfe/post/active-posts', user_auth(), post_controll.statistics);
router.get('/api/v1/cfe/post/search-plates/:value', user_auth(), post_controll.searchByPlates);
router.get('/api/v1/cfe/post/search/:value', user_auth(), post_controll.allPostsDepartment);
router.delete('/api/v1/cfe/post/delete/:id', user_auth(), post_controll.deletePost);
router.delete('/api/v1/cfe/post/delete-repair/:id', user_auth(), post_controll.deletePostRepair);

module.exports = router;