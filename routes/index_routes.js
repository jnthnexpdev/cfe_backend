const express = require('express');
const app = express();

app.use(require('./auth/auth_routes'));
app.use(require('./admin/admin_routes'));
app.use(require('./workshop/workshop_routes'));
app.use(require('./vehicle/vehicle_routes'));
app.use(require('./repair/repair_routes'));
app.use(require('./post/post_routes'));
app.use(require('./manager/manager_routes'));

module.exports = app;