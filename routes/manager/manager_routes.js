const express = require('express');
const manager_controll = require('../../controllers/manager/manager_controller');
const {admin_auth} = require('../../middlewares/admin_middleware');

const router = express.Router();

router.post('/api/v1/cfe/manager/create-account', manager_controll.createAccountManager);
router.get('/api/v1/cfe/manager/search/:id', admin_auth(), manager_controll.searchManager);

module.exports = router;