const express = require('express');
const cookie_parser = require('cookie-parser');
const router = express.Router();
const dotenv = require('dotenv');
dotenv.config();
const token_key = process.env.SECRET_KEY;
const { admin_auth } = require('../../middlewares/admin_middleware');
const { user_auth } = require('../../middlewares/auth_middleware');
const workshop_controll = require('../../controllers/workshop/workshop_controllers');

router.use(cookie_parser(token_key));

router.post('/api/v1/cfe/workshop/create-account', admin_auth(), workshop_controll.createWorkshop);
router.get('/api/v1/cfe/workshop/list', admin_auth(), workshop_controll.listWorkshops);
router.get('/api/v1/cfe/workshop/names', admin_auth(), workshop_controll.nameWorkshops);
router.get('/api/v1/cfe/workshop/search/:value', admin_auth(), workshop_controll.searchWorkshop);
router.get('/api/v1/cfe/workshop/search-id/:id', admin_auth(), workshop_controll.searchIdWorkshop);
router.get('/api/v1/cfe/workshop/repairs/:id', user_auth(), workshop_controll.getRepairs);
router.get('/api/v1/cfe/workshop/search-vehicle-repair/:id/:value', user_auth(), workshop_controll.searchVehiclesInRepairs);
router.get('/api/v1/cfe/workshop/repair/:id', user_auth(), workshop_controll.getRepairId);
router.put('/api/v1/cfe/workshop/edit/:id', admin_auth(), workshop_controll.editWorkshop);
router.delete('/api/v1/cfe/workshop/delete/:id', admin_auth(), workshop_controll.deleteWorkshop);

module.exports = router;