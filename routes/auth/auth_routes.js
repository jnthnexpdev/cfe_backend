const express = require('express');
const auth_controll = require('../../controllers/auth/auth_controllers');
const cookie_parser = require('cookie-parser');
const router = express.Router();
const {user_auth} = require('../../middlewares/auth_middleware');
const dotenv = require('dotenv');
dotenv.config();
const token_key = process.env.SECRET_KEY;

router.use(cookie_parser(token_key));

router.post('/api/v1/cfe/auth/login', auth_controll.loginUser);
router.get('/api/v1/cfe/auth/user/type', auth_controll.typeUser);
router.get('/api/v1/cfe/auth/user/department/:id', auth_controll.departmentManager);
router.get('/api/v1/cfe/auth/user/:id', user_auth(), auth_controll.infoUser);

module.exports = router;