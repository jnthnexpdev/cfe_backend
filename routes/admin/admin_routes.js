const express = require('express');
const admin_controll = require('../../controllers/admin/admin_controllers');
const {admin_auth} = require('../../middlewares/admin_middleware');

const router = express.Router();

router.post('/api/v1/cfe/admin/create-account', admin_controll.createAccount);
router.get('/api/v1/cfe/admin/search/:id', admin_auth(), admin_controll.searchAdmin);

module.exports = router;