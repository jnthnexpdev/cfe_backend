const express = require('express');
const repair_controll = require('../../controllers/repair/repair_controllers');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const path = require('path'); 
const cookie_parser = require('cookie-parser');
const router = express.Router();
const {admin_auth} = require('../../middlewares/admin_middleware');
const {user_auth} = require('../../middlewares/auth_middleware');
const dotenv = require('dotenv');
dotenv.config();
const token_key = process.env.SECRET_KEY;

router.use(cookie_parser(token_key));
router.use('/api/v1/cfe/uploads', user_auth(), express.static(path.join(__dirname, '../../uploads/')));

router.post('/api/v1/cfe/repair/create-repair', upload.array('Imagenes_Vehiculo'), user_auth(),  repair_controll.createRepair);
router.get('/api/v1/cfe/repair/list', user_auth(), repair_controll.detailsRepairs);
router.get('/api/v1/cfe/repair/stats', user_auth(), repair_controll.statistics);
router.get('/api/v1/cfe/repair/list-workshop/:value', user_auth(), repair_controll.detailsRepairsByWorkshop);
router.get('/api/v1/cfe/repair/list-without-workshop', user_auth(), repair_controll.repairsWithoutMechanic);
router.get('/api/v1/cfe/repair/posts', user_auth(), repair_controll.detailsRepairsPosts);
router.put('/api/v1/cfe/repair/add-step/:id', upload.array('Imagenes_Etapa'), user_auth(), repair_controll.addStep);
router.put('/api/v1/cfe/repair/update/:id', user_auth(), repair_controll.updateRepair);
router.put('/api/v1/cfe/repair/confirm-arrival', user_auth(), repair_controll.confirmArrival);
router.put('/api/v1/cfe/repair/submission/:id', user_auth(), repair_controll.updateSubmission);
router.put('/api/v1/cfe/repair/confirm-repair/:id_repair/:id_vehicle', user_auth(), repair_controll.confirmRepair);
router.delete('/api/v1/cfe/repair/delete/:id_repair/:placas/:workshop', user_auth(), repair_controll.deleteRepair);

module.exports = router;