const express = require('express');
const cookie_parser = require('cookie-parser');
const {user_auth} = require('../../middlewares/auth_middleware');
const {admin_auth} = require('../../middlewares/admin_middleware');
const vehicle_controll = require('../../controllers/vehicle/vehicle_controllers');
const router = express.Router();
const dotenv = require('dotenv');
dotenv.config();
const token_key = process.env.SECRET_KEY;

router.use(cookie_parser(token_key));

router.post('/api/v1/cfe/vehicle/new-vehicle', user_auth(), vehicle_controll.createVehicle);
router.get('/api/v1/cfe/vehicle/list', user_auth(), vehicle_controll.listVehicles);
router.get('/api/v1/cfe/vehicle/search/:value', user_auth(), vehicle_controll.searchVehicle);
router.get('/api/v1/cfe/vehicle/search/department/:value', user_auth(), vehicle_controll.searchVehicleDepartment);
router.get('/api/v1/cfe/vehicle/search-id/:id', user_auth(), vehicle_controll.searchIdVehicle);
router.put('/api/v1/cfe/vehicle/edit/:id', user_auth(), vehicle_controll.editVehicle);
router.delete('/api/v1/cfe/vehicle/delete/:id', user_auth(), vehicle_controll.deleteVehicle);


module.exports = router; 