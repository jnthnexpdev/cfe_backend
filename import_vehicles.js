const fs = require('fs');
const mongoose = require('mongoose');
const csv = require('csv-parser');
const vehicle_model = require('./models/vehicle/vehicle_model');
const dotenv = require('dotenv');
dotenv.config();

const connect = async () => {
    try {
        console.log('Attempting to connect to MongoDB...');
        await mongoose.connect('mongodb+srv://jnthnexpdev:jnthnexpdevadmintallercfe@taller.vdkr5yh.mongodb.net/?retryWrites=true&w=majority', { serverSelectionTimeoutMS: 100000 });
        console.log('Connected to MongoDB successfully');

        mongoose.connection.on('connected', () => {
            console.log('Connection event: MongoDB connected');
        });

        mongoose.connection.on('error', (err) => {
            console.log('Connection event: MongoDB error', err);
            throw new Error('Connection mongodb error');
        });
    } catch (err) {
        console.log('Error during MongoDB connection attempt', err);
        throw new Error('Connection mongodb error');
    }
};

async function createFolder(folder_name) {
    const folder_path = `./uploads/${folder_name}`;

    try {
        if (fs.existsSync(folder_path)) {
            console.log('La carpeta ya existe');
        } else {
            fs.mkdirSync(folder_path);
            console.log(`Carpeta ${folder_name} creada`);
        }
    } catch (err) {
        console.error('Error al crear la carpeta:', err);
    }
}

async function importVehicles() {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream('./datos.csv')
            .pipe(csv())
            .on('data', async (row) => {
                try {
                    await createFolder(row.Placas_Vehiculo.toUpperCase());

                    const newVehicle = new vehicle_model({
                        Economico_Vehiculo : row.Economico_Vehiculo,
                        Telefono_Encargado : 0,
                        RPE_Resguardante : row.RPE_Resguardante,
                        Nombre_Resguardante : row.Nombre_Resguardante,
                        Numero_Activo: row.Numero_Activo,
                        Division : row.Division,
                        Zona: row.Zona,
                        Departamento_Vehiculo : row.Departamento_Vehiculo,
                        Area: row.Area,
                        Centro_Trabajo : row.Centro_Trabajo,
            
                        Dias_Descanso : row.Dias_Descanso,
                        Financiamiento_Vehiculo: row.Financiamiento_Vehiculo,
                        Placas_Vehiculo : row.Placas_Vehiculo,
                        Traccion_Vehiculo: row.Traccion_Vehiculo,
                        Combustible_Vehiculo: row.Combustible_Vehiculo,
                        Uso_Vehiculo: row.Uso_Vehiculo,
                        Estatus_Vehiculo: row.Estatus_Vehiculo,
                        Estatus_MYSAP_Vehiculo: row.Estatus_MYSAP_Vehiculo,
                        Ubicacion_Fisica_Vehiculo: row.Ubicacion_Fisica_Vehiculo,
                        Verificacion_Vehiculo: row.Verificacion_Vehiculo,
                        Problema_Verificacion_Vehiculo: row.Problema_Verificacion_Vehiculo,
            
                        Marca_Chasis : row.Marca_Chasis,
                        Submarca_Chasis : row.Submarca_Chasis,
                        Modelo_Chasis: row.Modelo_Chasis,
                        Numero_Serie_Chasis : row.Numero_Serie_Chasis,
                        Tipo_Chasis : row.Tipo_Chasis,
            
                        Marca_Hidraulico: row.Marca_Hidraulico,
                        Tipo_Hidraulico : row.Tipo_Hidraulico,
                        Submarca_Hidraulico: row.Submarca_Hidraulico,
                        Modelo_Hidraulico: row.Modelo_Hidraulico,
            
                        Cantidad_Reparaciones : 0
                    });

                    await vehicle_model.create([newVehicle]);
                    console.log('Vehículo guardado en MongoDB');
                } catch (error) {
                    console.error('Error al guardar vehículo:', error);
                }
            })
    });
}

async function main() {
    try {
        connect();
        await importVehicles();
        console.log('Proceso de importación completado');
    } catch (error) {
        console.error('Error durante la importación:', error);
    }
}

main();