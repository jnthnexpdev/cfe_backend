const workshop_model = require('../../models/workshop/workshop_model');
const repair_model = require('../../models/repairs/repairs_model');
const vehicle_model = require('../../models/vehicle/vehicle_model');
const bcryptjs = require('bcryptjs');
const { getDate } = require('../../utils/date');
const fs = require('fs').promises;
const path = require('path');
const { Types } = require('mongoose');

exports.createWorkshop = async(req, res) => {
    const {Fecha, Hora} = await getDate();
    const body = req.body;

    try{
        const check_email  = await workshop_model.findOne({Email: body.Email});

        if(check_email){
            return res.status(401).json({
                ok : false,
                message : 'Este correo ya esta registrado.'
            });
        }

        const new_workshop = new workshop_model({
            Nombre : body.Nombre,
            Nombre_Responsable: body.Nombre_Responsable,
            Celular : body.Celular,
            Celular2: body.Celular2,
            Direccion: body.Direccion,
            Email : body.Email,
            Zona: body.Zona,
            Centro_Trabajo: body.Centro_Trabajo,
            Password : body.Password,
            Cuenta : 'Taller', //Tipo de cuenta Mecanico
            Fecha_Registro : Fecha,
            Hora_Registro : Hora,
            Notificationes : [],
            Reparaciones : []
        });

        await new_workshop.save();

        res.status(200).json({
            ok : true,
            message : 'Taller registrado',
            data : new_workshop
        });
    }catch(err){
        res.status(500).json({
            ok : false,
            message : 'Error al registrar el taller'
        })
    }
}

exports.listWorkshops = async (req, res) => {
    try {
        const page = parseInt(req.query.page) || 1; // Obtener el número de página desde los parámetros de consulta
        const pageSize = 20; // Especificar el tamaño de la página (número de talleres por página)

        const skip = (page - 1) * pageSize;

        const projection = {
            Nombre: 1,
            Nombre_Responsable: 1,
            Celular: 1,
            Celular2: 1,
            Direccion: 1,
            Email: 1,
            Zona: 1,
            Centro_Trabajo: 1
        };

        const workshops = await workshop_model.find({}, projection).skip(skip).limit(pageSize).lean();
        const totalWorkshops = await workshop_model.countDocuments();

        const totalPages = Math.ceil(totalWorkshops / pageSize);

        return res.status(200).json({
            ok: true,
            workshops: workshops,
            totalPages: totalPages,
            currentPage: page
        });
    } catch (err) {
        return res.status(500).json({
            ok: false,
            message: 'Error al obtener los talleres'
        });
    }
};

exports.nameWorkshops = async(req, res) => {
    try{
        const workshops = await getNameWorkshops();

        return res.status(200).json({
            ok : true,
            workshops : workshops
        });
    }catch(err){
        return res.status(500).json({
            ok : false,
            message: 'Error al obtener los datos de los talleres'
        });
    }
}

exports.getRepairs = async (req, res) => {
    try {
        // Id del taller
        const id = req.params.id;

        const workshop_repairs = await workshop_model.findById(id, { Reparaciones: 1 });

        if (!workshop_repairs) {
            return res.status(404).json({
                ok: false,
                message: 'No se han asignado reparaciones'
            });
        }

        const id_repairs = workshop_repairs.Reparaciones.map(repair => repair.Id_Reparacion);

        const data_repairs = await Promise.all(id_repairs.map(async (repair) => {
            const repair_info = await getDataRepairs(repair);
            return repair_info;
        }));

        const vehicles = await Promise.all(data_repairs.map(async (post) => {
            const vehicleInfo = await getDatavehicle(post.Id_Vehiculo);
            return vehicleInfo;
        })); 
        const fixedVehicles = vehicles.map(vehiclesArray => vehiclesArray[0]);

        // Obtener información de archivos para cada publicación
        const repairs = await Promise.all(data_repairs.map(async (repair) => {
            const folderPath = path.join(__dirname, `../../uploads/${repair.Carpeta_Documentos}/reparacion${repair.Fecha.toLowerCase()}`);
            const Files = await getFilesInFolder(folderPath);
            return { ...repair.toObject(), Files };
        }));

        return res.status(200).json({
            ok: true,
            repairs: repairs,
            vehicles : fixedVehicles
        });
 
    } catch (err) {
        console.error(err);
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
};

exports.searchVehiclesInRepairs = async (req, res) => {
    try {
        // Id del taller
        const id = req.params.id;
        const value = req.params.value;

        const workshopRepairs = await workshop_model.findById(id, { Reparaciones: 1 });

        if (!workshopRepairs) {
            return res.status(404).json({
                ok: false,
                message: 'No se han asignado reparaciones'
            });
        }

        const idRepairs = workshopRepairs.Reparaciones.map(repair => repair.Id_Reparacion);

        const dataRepairs = await Promise.all(idRepairs.map(async (repair) => {
            const repairInfo = await getDataRepairs(repair);
            return repairInfo;
        }));

        const vehicles = await Promise.all(dataRepairs.map(async (post) => {
            const vehicleInfo = await getDatavehicle(post.Id_Vehiculo);
            return vehicleInfo;
        }));

        // Aplanar el array de vehículos
        const flatVehicles = vehicles.flat();

        const filteredVehicles = flatVehicles.filter(vehicle => {
            if (vehicle) {
                const marcaChasis = vehicle.Marca_Chasis || '';
                const submarcaChasis = vehicle.Submarca_Chasis || '';
                return (
                    marcaChasis.toLowerCase().includes(value.toLowerCase()) ||
                    submarcaChasis.toLowerCase().includes(value.toLowerCase())
                );
            }
            return false;
        });

        const filteredVehicleIds = filteredVehicles.map(vehicle => vehicle._id);

        const filteredRepairs = await repair_model.find({
            Id_Vehiculo: { $in: filteredVehicleIds },
            Estatus : 'Activa'
        });

        const filteredRepairsWithFiles = await Promise.all(filteredRepairs.map(async (repair) => {
            const folderPath = path.join(__dirname, `../../uploads/${repair.Carpeta_Documentos}/reparacion${repair.Fecha.toLowerCase()}`);
            const files = await getFilesInFolder(folderPath);
            return { ...repair.toObject(), Files: files };
        }));

        console.log('Reparaciones filtradas con archivos: ', filteredRepairsWithFiles);

        return res.status(200).json({
            ok: true,
            vehicles: filteredVehicles,
            repairs: filteredRepairsWithFiles
        });

    } catch (err) {
        console.error(err);
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
};


exports.getRepairId = async (req, res) => {
    try {
        const id_repair = req.params.id;

        const data_repair = await getDataRepairs(id_repair);

        if (!data_repair) {
            return res.status(404).json({
                ok: false,
                message: 'No se ha encontrado la reparacion'
            });
        }

        const vehicle = await getDatavehicle(data_repair.Id_Vehiculo);
        const folderPath = path.join(__dirname, `../../uploads/${data_repair.Carpeta_Documentos}/reparacion${data_repair.Fecha.toLowerCase()}`);
        const files = await getFilesInFolder2(folderPath);

        return res.status(200).json({
            ok: true,
            repair: data_repair,
            vehicle : vehicle,
            files : files
        });

    } catch (err) {
        console.error(err);
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
}

exports.searchWorkshop = async(req, res) => {
    const value = req.params.value;

    try{
        const results = await getDataWorkshops(value); 

        return res.status(200).json({
            ok : true,
            workshops : results
        });

    }catch(err){
        return res.status(404).json({
            ok : false,
            message: 'No hay resultados'
        });
    }
}

exports.searchIdWorkshop = async(req, res) => {
    const id = req.params.id;

    const projection = { 
        Nombre: 1, 
        Nombre_Responsable: 1, 
        Celular: 1, 
        Celular2: 1, 
        Direccion: 1, 
        Email: 1, 
        Zona: 1, 
        Centro_Trabajo: 1
    };

    try{
        const workshop = await workshop_model.findById(id, projection).lean();

        if(!workshop){
            return res.status(404).json({
                ok : false,
                message : 'Taller no encontrado'
            });
        }

        return res.status(200).json({
            ok : true,
            workshop : workshop
        });

    }catch(err){
        return res.status(404).json({
            ok : true,
            message : 'Taller no encontrado'
        });
    }
}

exports.editWorkshop = async(req, res) => {
    const id = req.params.id;
    let fields = req.body;

    if (fields.Password && !fields.Password.startsWith('$2a$')) {
        const salt = await bcryptjs.genSalt(12);
        const hash = await bcryptjs.hash(fields.Password, salt);
        fields.Password = hash;
    }

    try{
        const update = await workshop_model.findOneAndUpdate({_id: id}, fields, {new : true});

        if(!update){
            return res.status(404).json({
                oK : false,
                message : 'Taller no encontrado'
            });
        }
        return res.status(200).json({
            ok : true,
            message : 'Informacion actualizada',
            workshop : update 
        });
    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error del servidor' 
        });
    }
}

exports.deleteWorkshop = async(req, res) => {
    const id = req.params.id;

    try{
        const drop =  await workshop_model.findByIdAndDelete(id);

        if(!drop){
            return res.status(404).json({
                ok : false,
                message : 'Taller no encontrado'
            }); 
        }

        return res.status(200).json({
            ok : true,
            message : 'Taller eliminado'
        });
    }catch(error){
        return res.status(500).json({
            ok : false,
            message : 'Error del servidor'
        });
    }
}

async function getDataWorkshops(value){

    const projection = { 
        Nombre: 1, 
        Nombre_Responsable: 1, 
        Celular: 1, 
        Direccion: 1, 
        Email: 1, 
        Zona: 1, 
        Centro_Trabajo: 1
    };

    try{
        const caseInsensitiveValue = new RegExp(value, 'i');

        const workshopData = await workshop_model.find({
            $or: [
                { Nombre : caseInsensitiveValue },
                { Nombre_Responsable : caseInsensitiveValue },
                { Celular : caseInsensitiveValue  },
                { Celular2 : caseInsensitiveValue },
                { Direccion : caseInsensitiveValue },
                { Email : caseInsensitiveValue  },
                { Zona : caseInsensitiveValue },
                { Centro_Trabajo : caseInsensitiveValue },
            ]
        }, projection).lean();

        return workshopData;

    }catch(err){
        throw new Error('Error al buscar el taller');
    }
}

async function getNameWorkshops(){

    const projection = {
        Nombre : 1,
        _id : 1
    };

    try{
        const name_workshops = await workshop_model.find({}, projection).lean();
        return name_workshops;
    }catch(err){
        throw new Error('Error al obtener los nombres de los talleres');
    }
}

async function getDataRepairs(id_repair){
    try{
        const repairs = await repair_model.findById(id_repair);
        return repairs;
    }catch(err){
        throw new Error('Error al obtener la informacion de la reparacion');
    }
}

async function getDatavehicle(id){
    try{
        const vehicles = await vehicle_model.find({_id : id});

        if(!vehicles){
            console.log('No hay vehiculos ');
        }

        return vehicles;
    }catch(err){    
        throw new Error('Error al obtener la informacion del vehiculo');
    }
}

async function getFilesInFolder(folderPath) {
    try {
        const files = await fs.readdir(folderPath);
        return files;
    } catch (error) {
        console.error('Error al obtener archivos en la carpeta:', error.message);
        return [];
    }
}

async function getFilesInFolder2(folderPath) {
    try {
        const files = await fs.readdir(folderPath);

        const publicationImages = [];
        const etapaImages = [];

        for (const file of files) {
            const filePath = path.join(folderPath, file);
            const stats = await fs.stat(filePath);

            if (stats.isFile()) {
                // Verificar si el archivo es una imagen
                const extname = path.extname(file).toLowerCase();
                if (['.jpeg', '.jpg', '.png', '.heif'].includes(extname)) {
                    publicationImages.push(file);
                }
            } else if (stats.isDirectory()) {
                // Si es un directorio, asumimos que es una carpeta de etapa
                const etapaFiles = await fs.readdir(filePath);
                etapaImages.push({
                    etapaFolder: file,
                    images: etapaFiles.filter(isImageFile)
                });
            }
        }

        return { publicationImages, etapaImages };
    } catch (error) {
        console.error('Error al obtener archivos en la carpeta:', error.message);
        return { publicationImages: [], etapaImages: [] };
    }
}

function isImageFile(file) {
    const extname = path.extname(file).toLowerCase();
    return ['.jpeg', '.jpg', '.png', '.heif'].includes(extname);
}
