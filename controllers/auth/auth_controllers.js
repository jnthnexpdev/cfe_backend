const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

const token_key = process.env.SECRET_KEY;
const admin_model = require('../../models/admin/admin_model');
const workshop_model = require('../../models/workshop/workshop_model');
const manager_model = require('../../models/manager/manager_model');

exports.loginUser = async(req, res) => {
    const {Username, Password} = req.body;

    try{
        const user = await findUser(Username);

        if(!user){ return res.status(404).json({ 
                ok : false, 
                message : 'Este usuario no existe'
            });
        }

        if(bcryptjs.compareSync(Password, user.Password)){
            const token = await createToken(user);

            res.cookie('token', token, {
                httpOnly : false,
                signed : true,
            });

            return res.status(200).json({
                ok : true,
                token : token
            });
        }else{
            return res.status(401).json({
                ok : false,
                message: 'Contraseña incorrecta'
            });
        }
    }catch(err){
        res.status(500).json({
            ok : false,
            message : 'Error del servidor',
        });
    }
}

exports.typeUser = async(req, res) => {
    try{
        const token = req.signedCookies.token;

        if(!token){
            return res.status(401).json({
                ok : false,
                message : 'No se ha proporcionado la cookie'
            });
        }

        const decoded = jwt.verify(token, token_key);
        const user = decoded.userType;

        return res.status(200).json({ user });

    }catch(err){
        res.status(500).json({
            ok : false,
            message : 'Error del servidor',
        });
    }
}

exports.infoUser = async(req, res) => {
    const id = req.params.id;

    try{
        const workshop = await dataUser(id);

        if(!workshop){
            return res.status(404).json({
                ok : false,
                message : 'Usuario no encontrado'
            });
        }

        return res.status(200).json({
            ok : true,
            workshop : workshop
        });

    }catch(err){
        res.status(500).json({
            ok : false,
            message : 'Error del servidor',
        });
    }
}

exports.departmentManager = async(req, res) => {
    const id = req.params.id;

    try{
        const manager = await departmentUser(id);

        if(!manager){
            return res.status(404).json({
                ok : false,
                message : 'Usuario no encontrado'
            });
        }

        return res.status(200).json({
            ok : true,
            manager : manager
        });

    }catch(err){
        res.status(500).json({
            ok : false,
            message : 'Error del servidor',
        });
    }
}

async function findUser(value){
    try{
        const admin = await admin_model.findOne({
            $or: [
                { Email : value},
                { RPE : value},
                { Celular : value }
            ]
        });
    
        const workshop = await workshop_model.findOne({
            $or: [

                { Email : value},
                { Celular : value },
                { Celular2 : value }
            ]
        });

        const manager = await manager_model.findOne({
            $or: [
                { Email : value},
                { RPE : value},
                { Celular : value }
            ]
        });

        return admin || workshop || manager;
    }catch(error){
        console.error(error);
        throw new Error('Error al buscar el usuario');
    }
}

async function dataUser(id){
    const workshop_fields = { 
        Nombre: 1, 
        Nombre_Responsable: 1, 
        Celular: 1, 
        Celular2: 1, 
        Direccion: 1, 
        Email: 1, 
        Zona: 1, 
        Centro_Trabajo: 1
    };

    try{
        const workshop = await workshop_model.findById(id, workshop_fields).lean();

        return workshop;
    }catch(err){
        console.error(error);
        throw new Error('Error al buscar el usuario');
    }
}

async function departmentUser(id){
    const fields = { 
        Nombre: 1, 
        Departamento : 1
    };

    try{
        const manager = await manager_model.findById(id, fields).lean();

        return manager;
    }catch(err){
        console.error(error);
        throw new Error('Error al buscar el usuario');
    }
}

async function createToken(user){
    const userType = user.Cuenta;

    const token = jwt.sign({
        userId: user._id,
        userType : userType
    },
    token_key,
    {expiresIn: '3h'},
    { algorithm: 'HS256' }
    );

    return token;
}