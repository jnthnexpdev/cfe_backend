const vehicle_model = require('../../models/vehicle/vehicle_model');
const {getDate} = require('../../utils/date');
const fs = require('fs');

exports.createVehicle = async(req, res) => {
    const body = req.body;

    try{
        const newVehicle = new vehicle_model({
            Economico_Vehiculo : body.Economico_Vehiculo,
            Telefono_Encargado : body.Telefono_Encargado,
            RPE_Resguardante : body.RPE_Resguardante,
            Nombre_Resguardante : body.Nombre_Resguardante,
            Numero_Activo: body.Numero_Activo,
            Division : body.Division,
            Zona: body.Zona,
            Departamento_Vehiculo : body.Departamento_Vehiculo,
            Area: body.Area,
            Centro_Trabajo : body.Centro_Trabajo,

            Dias_Descanso : body.Dias_Descanso,
            Financiamiento_Vehiculo: body.Financiamiento_Vehiculo,
            Placas_Vehiculo : body.Placas_Vehiculo,
            Traccion_Vehiculo: body.Traccion_Vehiculo,
            Combustible_Vehiculo: body.Combustible_Vehiculo,
            Uso_Vehiculo: body.Uso_Vehiculo,
            Estatus_Vehiculo: body.Estatus_Vehiculo,
            Estatus_MYSAP_Vehiculo: body.Estatus_MYSAP_Vehiculo,
            Ubicacion_Fisica_Vehiculo: body.Ubicacion_Fisica_Vehiculo,
            Verificacion_Vehiculo: body.Verificacion_Vehiculo,
            Problema_Verificacion_Vehiculo: body.Problema_Verificacion_Vehiculo,

            Marca_Chasis : body.Marca_Chasis,
            Submarca_Chasis : body.Submarca_Chasis,
            Modelo_Chasis: body.Modelo_Chasis,
            Numero_Serie_Chasis : body.Numero_Serie_Chasis,
            Tipo_Chasis : body.Tipo_Chasis,

            Marca_Hidraulico: body.Marca_Hidraulico,
            Tipo_Hidraulico : body.Tipo_Hidraulico,
            Submarca_Hidraulico: body.Submarca_Hidraulico,
            Modelo_Hidraulico: body.Modelo_Hidraulico,

            Cantidad_Reparaciones : 0
        });

        await createFolder(body.Placas_Vehiculo.toUpperCase());

        await newVehicle.save();

        res.status(200).json({
            ok : true,
            message : 'Vehiculo guardado',
            vehicle : newVehicle
        });

    }catch(err){
        console.error(err);
        res.status(500).json({
            ok : false,
            message : 'Error en el servidor',
        });
    }
}

exports.listVehicles = async (req, res) => {
    try {
        const page = parseInt(req.query.page) || 1;
        const pageSize = 50;

        const totalVehicles = await vehicle_model.countDocuments();
        const totalPages = Math.ceil(totalVehicles / pageSize);

        const skip = (page - 1) * pageSize;

        const vehicles = await vehicle_model.find()
            .skip(skip)
            .limit(pageSize);

        res.status(200).json({
            ok: true,
            vehicles: vehicles,
            currentPage: page,
            totalPages: totalPages
        });

    } catch (err) {
        res.status(500).json({
            ok: false,
            message: 'Error en el servidor',
        });
    }
}

exports.searchVehicle = async(req, res) => {
    const value = req.params.value;
    try{
        const vehicles = await getDataVehicle(value);

        if(!vehicles){
            return res.status(404).json({
                ok : true,
                message : 'No se han encontrado resultados'
            });
        }

        return res.status(200).json({
            ok : true,
            vehicles : vehicles
        });

    }catch(err){
        console.error(err);
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}

exports.searchVehicleDepartment = async(req, res) => {
    try {
        const value = req.params.value;
        console.log(value);
        const page = parseInt(req.query.page) || 1;
        const pageSize = 50;

        const totalVehicles = await vehicle_model.countDocuments();
        const totalPages = Math.ceil(totalVehicles / pageSize);

        const skip = (page - 1) * pageSize;

        const vehicles = await vehicle_model.find({Departamento_Vehiculo : value})
            .skip(skip)
            .limit(pageSize);

        res.status(200).json({
            ok: true,
            vehicles: vehicles,
            currentPage: page,
            totalPages: totalPages
        });

    } catch (err) {
        res.status(500).json({
            ok: false,
            message: 'Error en el servidor',
        });
    }
}

exports.searchIdVehicle = async(req, res) => {

    const id = req.params.id;

    try{
        const vehicle = await vehicle_model.findById(id);

        if(!vehicle){
            return res.status(404).json({
                ok : false,
                message : 'Vehiculo no encontrado'
            });
        }

        return res.status(200).json({
            ok : true,
            vehicle : vehicle
        });

    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}

exports.editVehicle = async(req, res) => {
    const id = req.params.id;
    const fields = req.body;

    try{

        const update = await vehicle_model.findOneAndUpdate({_id : id}, fields, {new : true});

        if(!update){
            return res.status(404).json({
                ok : false,
                message : 'Vehiculo no encontrado',
            });
        }

        return res.status(200).json({
            ok : true,
            message : 'Vehiculo editado',
        });

    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor',
        });
    }
}

exports.deleteVehicle = async(req, res) => {
    const id = req.params.id;

    try{
        const drop = await vehicle_model.findByIdAndDelete(id);

        if(!drop){
            return res.status(404).json({
                ok : false,
                message : 'No se ha encontrado el vehiculo'
            });
        }

        return res.status(200).json({
            ok : true,
            message : 'Vehiculo eliminado'
        });

    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}



async function getDataVehicle(value){
    try{
        const caseInsensitiveValue = new RegExp(value, 'i');

        const vehicleData = await vehicle_model.find({
            $or: [
                { Placas_Vehiculo : caseInsensitiveValue },
                { Numero_Serie_Chasis : caseInsensitiveValue },
                { Economico_Vehiculo : caseInsensitiveValue },
                { Marca_Chasis : caseInsensitiveValue },
                { Submarca_Chasis : caseInsensitiveValue },
                { RPE_Resguardante : caseInsensitiveValue }
            ]
        });

        return vehicleData;

    } catch (err) {
        console.error('Error en getDataVehicle:', err);
        throw new Error('Error al buscar el vehiculo');
    }
}

async function createFolder(folder_name) {
    const folder_path = `./uploads/${folder_name}`;

    try {
        if (fs.existsSync(folder_path)) {
            console.log('La carpeta ya existe');
        } else {
            fs.mkdirSync(folder_path);
            console.log(`Carpeta ${folder_name} creada`);
        }
    } catch (err) {
        console.error('Error al crear la carpeta:', err);
    }
}