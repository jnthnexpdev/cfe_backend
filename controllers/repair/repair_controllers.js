const repair_model = require('../../models/repairs/repairs_model');
const vehicle_model = require('../../models/vehicle/vehicle_model');
const post_model = require('../../models/posts/post_model');
const admin_model = require('../../models/admin/admin_model');
const workshop_model = require('../../models/workshop/workshop_model');
const fs = require('fs');
const path = require('path'); 
const nodemailer = require('nodemailer');
const dotenv = require('dotenv');
dotenv.config();

const { promisify } = require('util');
const mkdirAsync = promisify(fs.mkdir);
const { getDate2 } = require('../../utils/date');
const Email_Key = process.env.EMAIL_KEY;

exports.createRepair = async (req, res) => {
    const body = req.body;

    try {
        const { Fecha } = await getDate2();
        const id_vehiculo = await getIdVehicle(body.Placas_Vehiculo);
        const vehicle_data = await getDatavehicle(id_vehiculo);
    
        if (vehicle_data[0].Estatus_Vehiculo === 'Descompuesto') {
            return res.status(409).json({
                ok: false,
                message: 'Este vehiculo ya esta en proceso de reparacion'
            });
        }
    
        const new_repair = new repair_model({
            Id_Mecanico: null,
            Id_Administrador: body.Id_Administrador,
            Id_Vehiculo: id_vehiculo,
            Fecha_Problema: body.Fecha_Problema,
            Fecha: Fecha,
            Estatus: 'Activa',
            Diagnostico_Previo: body.Diagnostico_Previo,
            Presupuesto_Inicial: 0,
            Presupuesto_Final: 0,
            Carpeta_Documentos: body.Placas_Vehiculo,
        });
    
        await new_repair.save();
    
        await createFolder(new_repair.Carpeta_Documentos, Fecha);
    
        if (req.files && req.files.length > 0 && vehicle_data[0].Estatus_Vehiculo === 'En servicio') {
            console.log('Procesando archivos...');
            await processFiles(req.files, new_repair.Carpeta_Documentos, Fecha);
        } else {
            console.log('Condiciones no cumplidas para procesar archivos.');
        }
    
        await vehicle_model.findByIdAndUpdate(id_vehiculo, {
            Estatus_Vehiculo: 'Descompuesto',
            Estatus_MYSAP_Vehiculo: 'Descompuesto'
        }, { new: true });
    
        await updateRepairLog(new_repair.Id_Vehiculo, new_repair._id);
    
        await createPost(body, new_repair._id);
    
        await sendEmails(vehicle_data, body.Diagnostico_Previo);
    
        return res.status(200).json({
            ok: true,
            message: 'Reparacion guardada',
            repair: new_repair,
        });
    } catch (err) {
        console.error(err);
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor',
        });
    }
    
};

exports.statistics = async (req, res) => {
    try {
        const concluidas = await repair_model.countDocuments({Estatus : 'Concluida'});
        const activas = await repair_model.countDocuments({Estatus : 'Activa'});

        return res.status(200).json({
            ok: true,
            concluidas: concluidas,
            activas : activas
        });
    } catch (err) {
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
};

exports.detailsRepairs = async (req, res) => {
    
    try {
        const page = parseInt(req.query.page) || 1; // Obtener el número de página desde los parámetros de consulta
        const pageSize = 20; // Especificar el tamaño de la página (número de registros por página)

        const skip = (page - 1) * pageSize;

        const totalRecords = await repair_model.countDocuments({ Estatus: 'Activa' });
        const totalPages = Math.ceil(totalRecords / pageSize);

        const repairs = await repair_model.find({ Estatus: 'Activa' })
            .skip(skip)
            .limit(pageSize);

        if(!repairs || repairs.length === 0){
            return res.status(200).json({
                ok : true,
                message : 'Aun no hay reparaciones',
                repairs : false
            });
        }

        //Admin info 
        const admins = await Promise.all(repairs.map(async (repair) => {
            const adminInfo = await getDataAdmin(repair.Id_Administrador);
            return adminInfo;
        }));
        const fixedAdmins = admins.map(adminArray => adminArray[0]);

        //Vehicle info
        const vehicles = await Promise.all(repairs.map(async (repair) => {
            const vehicleInfo = await getDatavehicle(repair.Id_Vehiculo);
            return vehicleInfo;
        }));
        const fixedVehicles = vehicles.map(vehiclesArray => vehiclesArray[0]);

        //Workshops info
        const workshops = await Promise.all(repairs.map(async (repair) => {
            const workshopInfo = await getDataWorkshop(repair.Id_Mecanico);
            return workshopInfo;
        }));
        const fixedWorkshops = workshops.map(workshopsArray => workshopsArray[0]);

        return res.status(200).json({
            ok: true,
            repairs: repairs,
            admins: fixedAdmins,
            vehicles : fixedVehicles,
            workshops : fixedWorkshops,
            totalPages: totalPages,
            currentPage: page
        });

    } catch(err) {
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}

exports.detailsRepairsByWorkshop = async (req, res) => {
    try {
        const page = parseInt(req.query.page) || 1;
        const pageSize = 20;
        const skip = (page - 1) * pageSize;

        const nombreTaller = req.params.value; // Obtén el nombre del taller de los parámetros de la URL
        console.log(nombreTaller)
        const workshop = await workshop_model.findOne({ Nombre: { $regex: new RegExp(nombreTaller, 'i') } });

        if (!workshop) {
            return res.status(404).json({
                ok: true,
                message: 'No se encontró el taller',
                repairs: false
            });
        }

        const totalRecords = await repair_model.countDocuments({ Estatus: 'Activa', 'Id_Mecanico': workshop._id });
        const totalPages = Math.ceil(totalRecords / pageSize);

        const repairs = await repair_model.find({ Estatus: 'Activa', 'Id_Mecanico': workshop._id })
            .skip(skip)
            .limit(pageSize);

        if (!repairs || repairs.length === 0) {
            return res.status(200).json({
                ok: true,
                message: 'Aún no hay reparaciones para este taller',
                repairs: false
            });
        }

        //Admin info 
        const admins = await Promise.all(repairs.map(async (repair) => {
            const adminInfo = await getDataAdmin(repair.Id_Administrador);
            return adminInfo;
        }));
        const fixedAdmins = admins.map(adminArray => adminArray[0]);

        //Vehicle info
        const vehicles = await Promise.all(repairs.map(async (repair) => {
            const vehicleInfo = await getDatavehicle(repair.Id_Vehiculo);
            return vehicleInfo;
        }));
        const fixedVehicles = vehicles.map(vehiclesArray => vehiclesArray[0]);

        //Workshops info
        const workshops = await Promise.all(repairs.map(async (repair) => {
            const workshopInfo = await getDataWorkshop(repair.Id_Mecanico);
            return workshopInfo;
        }));
        const fixedWorkshops = workshops.map(workshopsArray => workshopsArray[0]);

        return res.status(200).json({
            ok: true,
            repairs: repairs,
            admins: fixedAdmins,
            vehicles: fixedVehicles,
            workshops: fixedWorkshops,
            totalPages: totalPages,
            currentPage: page
        });

    } catch (err) {
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
}

exports.repairsWithoutMechanic = async (req, res) => {
    try {
        const page = parseInt(req.query.page) || 1;
        const pageSize = 20;
        const skip = (page - 1) * pageSize;

        const totalRecords = await repair_model.countDocuments({ Estatus: 'Activa', Id_Mecanico : null});
        const totalPages = Math.ceil(totalRecords / pageSize);

        const repairs = await repair_model.find({ Estatus: 'Activa', Id_Mecanico : null })
            .skip(skip)
            .limit(pageSize);

        if (!repairs || repairs.length === 0) {
            return res.status(200).json({
                ok: true,
                message: 'No hay reparaciones sin asignar mecánico',
                repairs: false
            });
        }

        //Admin info 
        const admins = await Promise.all(repairs.map(async (repair) => {
            const adminInfo = await getDataAdmin(repair.Id_Administrador);
            return adminInfo;
        }));
        const fixedAdmins = admins.map(adminArray => adminArray[0]);

        //Vehicle info
        const vehicles = await Promise.all(repairs.map(async (repair) => {
            const vehicleInfo = await getDatavehicle(repair.Id_Vehiculo);
            return vehicleInfo;
        }));
        const fixedVehicles = vehicles.map(vehiclesArray => vehiclesArray[0]);

        //Workshops info
        const workshops = await Promise.all(repairs.map(async (repair) => {
            const workshopInfo = await getDataWorkshop(repair.Id_Mecanico);
            return workshopInfo;
        }));
        const fixedWorkshops = workshops.map(workshopsArray => workshopsArray[0]);

        return res.status(200).json({
            ok: true,
            repairs: repairs,
            admins: fixedAdmins,
            vehicles : fixedVehicles,
            workshops : fixedWorkshops,
            totalPages: totalPages,
            currentPage: page
        });

    } catch (err) {
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
};

exports.addStep = async (req, res) => {
    const body = req.body;

    try{
        const {Fecha, Hora} = await getDate2();
        const id_repair = req.params.id;
        const repair = await repair_model.findById(id_repair);

        const Etapa = {
            Titulo_Etapa : body.Titulo_Etapa,
            Fecha_Etapa: Fecha,
            Hora_Etapa : Hora,
            Descripcion_Etapa : body.Descripcion_Etapa,
            Carpeta_Documentos_Etapa: `etapa${Fecha}H${Hora[0]}${Hora[1]}${Hora[3]}${Hora[4]}`,
        }

        const update = await repair_model.findByIdAndUpdate(id_repair, {
            $push : {
                Etapa_Reparacion : {
                    Titulo_Etapa : body.Titulo_Etapa,
                    Fecha_Etapa: Fecha,
                    Hora_Etapa : Hora,
                    Descripcion_Etapa : body.Descripcion_Etapa,
                    Carpeta_Documentos_Etapa: `etapa${Fecha}H${Hora[0]}${Hora[2]}${Hora[3]}`,
                }
            }
        },
        {new : true}
        );

        if(!update){
            return res.status(400).json({
                ok : false,
                message : 'Error al agregar la etapa'
            });
        }

        const folder_repair = `reparacion${repair.Fecha}`;

        await createSubFolder(repair.Carpeta_Documentos, folder_repair, Etapa.Carpeta_Documentos_Etapa);

        if (req.files && req.files.length > 0) {
            console.log('Procesando archivos...');
            await processFilesStep(req.files, repair.Carpeta_Documentos, folder_repair, Etapa.Carpeta_Documentos_Etapa);
        } else {
            console.log('Condiciones no cumplidas para procesar archivos.');
        }
 
        return res.status(200).json({
            ok : true,
            message : 'Etapa agregada'
        });

    }catch(err){
        console.log(err);
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}

exports.detailsRepairsPosts = async (req, res) => {
    
    try{
        const repairs = await repair_model.find();

        if(!repairs || repairs.length === 0){
            return res.status(200).json({
                ok : true,
                message : 'Aun no hay reparaciones',
                repairs : false
            });
        }

        //Admin info 
        const admins = await Promise.all(repairs.map(async (repair) => {
            const adminInfo = await getDataAdmin(repair.Id_Administrador);
            return adminInfo;
        }));
        const fixedAdmins = admins.map(adminArray => adminArray[0]);

        //Vehicle info
        const vehicles = await Promise.all(repairs.map(async (repair) => {
            const vehicleInfo = await getDatavehicle(repair.Id_Vehiculo);
            return vehicleInfo;
        }));
        const fixedVehicles = vehicles.map(vehiclesArray => vehiclesArray[0]);

        return res.status(200).json({
            ok: true,
            repairs: repairs,
            admins: fixedAdmins,
            vehicles : fixedVehicles
        });

    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error inteno del servidor'
        });
    }
}

exports.updateRepair = async (req, res) => {
    try {
        const id = req.params.id;
        const body = req.body;

        const data_repair = await repair_model.findById(id);

        const repair = await repair_model.findByIdAndUpdate(id, {
            Presupuesto_Inicial: body.Presupuesto_Inicial,
            Id_Mecanico: body.Id_Mecanico
        }, { new: true });

        await addRepairToWorkshop(id, body.Id_Mecanico);
        await changeStatusPost(id);
        await vehicle_model.findByIdAndUpdate(data_repair.Id_Vehiculo, {Estatus_Vehiculo : 'Taller', Estatus_MYSAP_Vehiculo : 'Taller'}, {new : true});
        const workshop = await workshop_model.findById(body.Id_Mecanico);

        await sendEmailToWorkshop(workshop);

        return res.status(200).json({
            ok: true,
            message: 'Reparacion actualizada',
            repair: repair
        });

    } catch (err) {
        console.error(err);
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
};

exports.confirmArrival = async (req, res) => {
    const body = req.body;
    try{
        const id_repair = body.Id_Reparacion;
        const {Fecha} = await getDate2();

        const asingDate = await repair_model.findByIdAndUpdate(id_repair, {Fecha_Taller : Fecha}, {new : true});

        return res.status(200).json({
            ok : true,
            message : 'Confirmacion recibida'
        });

    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}

exports.updateSubmission = async (req, res) => {
    const body = req.body;
    const id_repair = req.params.id;
    try{

        const repair = await repair_model.findByIdAndUpdate(id_repair, 
            { 
                Fecha_Entrega : body.Fecha_Entrega,
                Presupuesto_Final : body.Presupuesto_Final
            },
            { new : true}
        );

        if(!repair){
            return res.status(404).json({
                ok : false,
                message : 'Reparacion no encontrada'
            });
        }

        return res.status(200).json({
            ok : true,
            message : 'Informacion actualizada'
        });
    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor' 
        });
    }
}

exports.confirmRepair = async (req, res) => {
    try{
        const id_vehicle = req.params.id_vehicle;
        const id_repair = req.params.id_repair;

        await vehicle_model.findByIdAndUpdate(id_vehicle , 
            {
                Estatus_Vehiculo : 'En servicio',
                Estatus_MYSAP_Vehiculo : 'En servicio'
            }, {new : true}
        );

        await repair_model.findByIdAndUpdate(id_repair, 
            {
                Estatus : 'Concluida'
            }, {new : true}
        );

        return res.status(200).json({
            ok : true,
            message : 'Reparacion concluida'
        });
        
    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor' 
        });
    }
}

exports.deleteRepair = async (req, res) => {
    const id_repair = req.params.id_repair;
    const placas = req.params.placas;
    const id_workshop = req.params.workshop;
    
    try{
        const id_vehicle = await getIdVehicle(placas);
        const update_vehicle = await deleteRepairAndUpdateLog(id_vehicle, id_repair);
 
        if(id_workshop !== 'null'){
            await removeRepairFromWorkshop(id_repair, id_workshop);
        }

        const delete_repair = await repair_model.findByIdAndDelete(id_repair);

        if(!delete_repair){
            return res.status(404).json({
                ok : false,
                message : 'Reparacion no encontrada'
            });
        }

        return res.status(200).json({
            ok : true,
            message : 'Reparacion eliminada'
        });
    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor' 
        });
    }
}

async function getDataAdmin(id){
    const projection = {
        Nombre : 1,
        Email : 1,
        RPE : 1,
        Celular : 1,
        Fecha_Registro : 1,
        Hora_Registro : 1
    };

    try{
        const admins = await admin_model.find({_id : id}, projection);

        return admins;
    }catch(err){
        throw new Error('Administrado no encontrado');
    }
}

async function getIdVehicle(value) {
    const caseInsensitiveValue = new RegExp(value, 'i');

    try {
        const vehicle = await vehicle_model.findOne(
            { Placas_Vehiculo: caseInsensitiveValue },
            { _id: 1 }
        ).lean();

        if (vehicle) {
            return vehicle._id;
        } else {
            throw new Error('Vehículo no encontrado');
        }
    } catch (err) {
        throw new Error('Error al buscar el vehiculo');
    }
}

async function getDatavehicle(id){
    try{
        const vehicles = await vehicle_model.find({_id : id});

        if(!vehicles){
            console.log('No hay vehiculos ');
        }

        return vehicles;
    }catch(err){    
        throw new Error('Error al obtener la informacion del vehiculo');
    }
}

async function getDepartmentVehicle(id) {
    try {
        const vehicle = await vehicle_model.findOne(
            { _id: id },
            { Departamento_Vehiculo: 1 }
        ).lean();

        if (vehicle) {
            return vehicle.Departamento_Vehiculo;
        } else {
            throw new Error('Vehículo no encontrado para el ID proporcionado');
        }
    } catch (err) {
        console.error('Error al buscar el vehículo:', err);
        throw new Error('Error al buscar el vehiculo');
    }
}

async function createPost(body, id){
    try{
        const { Fecha, Hora } = await getDate2();
        const id_vehiculo = await getIdVehicle(body.Placas_Vehiculo);
        const departamento = await getDepartmentVehicle(id_vehiculo); 
        
        const new_post = new post_model({
            Id_Administrador: body.Id_Administrador,
            Id_Vehiculo: id_vehiculo,
            Id_Reparacion : id,
            Contenido_Publicacion : body.Diagnostico_Previo,
            Fecha_Publicacion: Fecha,
            Hora_Publicacion: Hora,
            Carpeta_Documentos: body.Placas_Vehiculo,
            Estatus : 'Activa',
            Departamento : departamento,
            Comentarios : []
        }); 

        await new_post.save();

        console.log('Publicacion creada');
    }catch(err){
        console.error(err);
        throw new Error('Error al crear la publicacion');
    }
}

async function sendEmails(vehiculo, diagnostico){

    const transporter_email = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'cfetallerdev@gmail.com',
          pass: Email_Key
        }
    });

    try {
        const emails = await getEmailsWorkshop();

        const options_email = {
            from: 'questionatec@gmail.com',
            // Destinatarios como cadena separada por comas
            to: emails.join(', '),
            subject: 'Nuevo vehiculo para reparacion',
            html: `
            <div style="font-family: Arial, sans-serif; color: #000;">
                <h2 style="color: #008e60; text-align : center;">¡Nuevo vehículo para reparación!</h2>
                
                <p style="font-weight: bold"><strong>Modelo:</strong> ${vehiculo[0].Marca_Chasis}</p>
                <p style="font-weight: bold"><strong>Submodelo:</strong> ${vehiculo[0].Submarca_Chasis}</p>
                <p style="font-weight: bold"><strong>Marca:</strong> ${vehiculo[0].Modelo_Chasis}</p>
                <p style="font-weight: bold"><strong>Zona:</strong> ${vehiculo[0].Zona}</p>
                <p style="font-weight: bold"><strong>Division:</strong> ${vehiculo[0].Division}</p>
                <p style="font-weight: bold"><strong>Descripción del problema mecánico:</strong></p>
                <p>${diagnostico}</p>
            </div>
            `,
        };

        transporter_email.sendMail(options_email, function (error, info) {
            if (error) {
                throw new Error('Error al enviar el correo: ' + error.message);
            } else {
                console.log('Correo enviado: ' + info.response);
            }
        });
    } catch (err) {
        console.error(err.message);
    } 

}

//new repair 
async function sendEmailToWorkshop(workshop){
    const transporter_email = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'cfetallerdev@gmail.com',
          pass: Email_Key
        }
    });

    try {
        const email_address = workshop.Email;

        const options_email = {
            from: 'questionatec@gmail.com',
            // Destinatarios como cadena separada por comas
            to: email_address,
            subject: 'Nuevo vehiculo asignado',
            html: `
            <div style="font-family: Arial, sans-serif; color: #000;">
                <h2 style="color: #008e60; text-align : center;">¡Nuevo vehículo para reparación!</h2>
                
                <p style="font-weight: bold"> 
                    Hola ${workshop.Nombre}, te han asignado un nuevo 
                    vehiculo para su reparacion, puedes ver mas detalles en la pagina web. 
                </p>
            </div>
            `,
        };

        transporter_email.sendMail(options_email, function (error, info) {
            if (error) {
                throw new Error('Error al enviar el correo: ' + error.message);
            } else {
                console.log('Correo enviado: ' + info.response);
            }
        });
    } catch (err) {
        console.error(err.message);
    } 
}

async function getEmailsWorkshop(){
    try{
        const workshops = await workshop_model.find({});

        const emails = workshops.map(workshop => workshop.Email);

        console.log(emails);

        return emails;
    }catch(err){
        throw new Error('Error al obtener los correos');
    }
}

async function getDataWorkshop(id){
    const projection = {
        _id : 1,
        Nombre : 1
    };

    try{
        const workshops = await workshop_model.find({_id : id}, projection);

        return workshops;
    }catch(err){
        throw new Error('Error al buscar el taller');
    }
}

async function updateRepairLog(id_vehiculo, id_repair){

    try{
        await vehicle_model.findByIdAndUpdate(id_vehiculo, {
            $addToSet : {
                Historial_Reparaciones_Vehiculo : {
                    Id_Reparacion : id_repair,
                }
            },
            $inc : { Cantidad_Reparaciones: 1 }, 
        }, {new : true});
    }catch(err){
        console.error('Error al actualizar el historial del vehiculo')
    }
}

async function deleteRepairAndUpdateLog(id_vehiculo, id_repair) {
    try {
        // Obtener el contador actual
        const vehicle = await vehicle_model.findById(id_vehiculo);
        const currentCount = vehicle.Cantidad_Reparaciones;

        // Verificar que el contador sea mayor a cero antes de decrementarlo
        if (currentCount > 0) {
            // Eliminar la reparación del historial y decrementar el contador
            await vehicle_model.findByIdAndUpdate(id_vehiculo, {
                $pull: {
                    Historial_Reparaciones_Vehiculo: {
                        Id_Reparacion: id_repair,
                    },
                },
                $inc: { Cantidad_Reparaciones: -1 }, // Decrementar el contador
            }, { new: true });

            console.log('Reparación eliminada y contador decrementado');
        } else {
            console.log('El contador ya es cero, no se decrementa');
        }
    } catch (err) {
        console.error('Error al actualizar el historial del vehículo', err);
        throw err;
    }
}

async function createFolder(vehicle_folder, date_repair) {
    const folder_path = `./uploads/${vehicle_folder}`;

    try {
        // Verificar si existe la carpeta con las placas del vehiculo
        if (fs.existsSync(folder_path)) {
            const subfolder_path = `${folder_path}/reparacion${date_repair}`;
            // Verificar si no existe una carpeta igual
            if (fs.existsSync(subfolder_path)) {
                console.log('La subcarpeta ya existe');
            }
            // Crear la carpeta de la reparacion
            else {
                await mkdirAsync(subfolder_path);
                console.log(`Subcarpeta creada: ${subfolder_path}`);
            }
        } else {
            console.log('La carpeta no existe');
        }
    } catch (err) {
        console.error('Error al crear la carpeta:', err);
    }
}

async function processFiles(files, vehicle_folder, date_repair) {
    const destinationPath = path.join(__dirname, `../../uploads/${vehicle_folder}/reparacion${date_repair}`);

    for (const file of files) {
        const originalName = file.originalname.replace(/\s+/g, '');
        const extname = path.extname(originalName).toLowerCase();

        // Verificar si la extensión es válida
        if (['.jpeg', '.jpg', '.png', '.heif'].includes(extname)) {
            const filePath = path.join(destinationPath, originalName);

            // Mover el archivo de la carpeta temporal a la carpeta específica
            await fs.promises.rename(file.path, filePath);
        } else {
            console.log(`Archivo no permitido: ${originalName}`);
        }
    }
}

async function createSubFolder(plates, repair_folder, step_folder) {
    const folder_path = `./uploads/${plates}/${repair_folder}`;

    try {
        // Verificar si existe la carpeta con las placas del vehiculo
        if (fs.existsSync(folder_path)) {
            const subfolder_path = `${folder_path}/${step_folder}`;
            if (fs.existsSync(subfolder_path)) {
                console.log('La subcarpeta ya existe');
            }
            else {
                await mkdirAsync(subfolder_path);
                console.log(`Subcarpeta creada: ${subfolder_path}`);
            }
        } else {
            console.log('La carpeta no existe');
        }
    } catch (err) {
        console.error('Error al crear la carpeta:', err);
    }
}


async function processFilesStep(files, folder, repair_folder, step_folder) {
    const destinationPath = path.join(__dirname, `../../uploads/${folder}/${repair_folder}/${step_folder}`);
    for (const file of files) {
        const originalName = file.originalname.replace(/\s+/g, '');
        const extname = path.extname(originalName).toLowerCase();

        // Verificar si la extensión es válida
        if (['.jpeg', '.jpg', '.png', '.heif'].includes(extname)) {
            const filePath = path.join(destinationPath, originalName);

            // Mover el archivo de la carpeta temporal a la carpeta específica
            await fs.promises.rename(file.path, filePath);
        } else {
            console.log(`Archivo no permitido: ${originalName}`);
        }
    }
}

async function removeRepairFromWorkshop(id_repair, id_workshop) {
    try {
        const workshop = await workshop_model.findByIdAndUpdate(id_workshop, {
            $pull: {
                Reparaciones: {
                    Id_Reparacion: id_repair
                }
            }
        }, { new: true });

        console.log('Reparacion eliminada del taller');
    } catch (err) {
        console.error('Error al eliminar la reparacion del taller:', err);
        throw new Error('Error al eliminar la reparacion del taller');
    }
}

async function addRepairToWorkshop(id_repair, id_workshop) {
    try {
        const workshop = await workshop_model.findByIdAndUpdate(id_workshop, {
            $push: {
                Reparaciones : {
                    Id_Reparacion: id_repair
                }
            }
        }, { new: true });

        console.log('Reparacion asignada');
    } catch (err) {
        console.error('Error al asignar la reparacion al taller:', err);
        throw new Error('Error al asignar la reparacion al taller');
    }
}

async function changeStatusPost(id_repair) {
    try {
        const update = await post_model.findOneAndUpdate({ Id_Reparacion: id_repair }, { Estatus: 'Pausada' }, { new: true });
        console.log('Estatus de la publicacion actualizado');
    } catch (err) {
        console.error('Error al actualizar el estatus de la publicacion:', err);
        throw new Error('Error al actualizar el estatus de la publicacion');
    }
}