const post_model = require('../../models/posts/post_model');
const vehicle_model = require('../../models/vehicle/vehicle_model');
const admin_model = require('../../models/admin/admin_model');
const workshop_model = require('../../models/workshop/workshop_model');
const { getDate2 } = require('../../utils/date');
const fs = require('fs').promises;
const path = require('path');

exports.allPosts = async (req, res) => {
    try {
        const page = parseInt(req.query.page) || 1;
        const perPage = 10;

        const startIndex = (page - 1) * perPage;
        const endIndex = page * perPage;

        const totalPosts = await post_model.countDocuments({ Estatus: 'Activa' });

        const posts = await post_model.find({ Estatus: 'Activa' })
            .skip(startIndex)
            .limit(perPage);

        if (!posts || posts.length === 0) {
            return res.status(404).json({
                ok: false,
                message: 'No hay publicaciones'
            });
        }

        // Admin info 
        const admins = await Promise.all(posts.map(async (post) => {
            const adminInfo = await getDataAdmin(post.Id_Administrador);
            return adminInfo;
        }));
        const fixedAdmins = admins.map(adminArray => adminArray[0]);

        // Vehicle info
        const vehicles = await Promise.all(posts.map(async (post) => {
            const vehicleInfo = await getDatavehicle(post.Id_Vehiculo);
            return vehicleInfo;
        }));
        const fixedVehicles = vehicles.map(vehiclesArray => vehiclesArray[0]);

        // Obtener información de archivos para cada publicación
        const postsWithFiles = await Promise.all(posts.map(async (post) => {
            const folderPath = path.join(__dirname, `../../uploads/${post.Carpeta_Documentos}/reparacion${post.Fecha_Publicacion.toLowerCase()}`);
            const files = await getFilesInFolder(folderPath);
            return { ...post.toObject(), files };
        }));

        // Obtener los nombres de los usuarios que han comentado
        const users = await Promise.all(posts.map(async (post) => {
            if (Array.isArray(post.Comentarios) && post.Comentarios.length > 0) {
                const userNames = await Promise.all(post.Comentarios.map(async (comment) => {
                    if (comment.Id_Usuario) {
                        return await getNameUser(comment.Id_Usuario);
                    }
                    return null;
                }));
                return userNames;
            }
            return null;
        }));

        return res.status(200).json({
            ok: true,
            posts: postsWithFiles,
            admins: fixedAdmins,
            vehicles: fixedVehicles,
            users: users,
            currentPage: page,
            totalPages: Math.ceil(totalPosts / perPage)
        });

    } catch (err) {
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
}

exports.allPostsDepartment = async (req, res) => {
    try {
        const value = req.params.value;

        const page = 1;
        const perPage =  10;
        const startIndex = (page - 1) * perPage;
        const endIndex = page * perPage;

        const totalPosts = await post_model.countDocuments({ Estatus: 'Activa', Departamento : value});

        const posts = await post_model.find({ Estatus: 'Activa', Departamento : value})
            .skip(startIndex)
            .limit(perPage);

        if (!posts || posts.length === 0) {
            return res.status(404).json({
                ok: false,
                message: 'No hay publicaciones'
            });
        }

        // Admin info 
        const admins = await Promise.all(posts.map(async (post) => {
            const adminInfo = await getDataAdmin(post.Id_Administrador);
            return adminInfo;
        }));
        const fixedAdmins = admins.map(adminArray => adminArray[0]);

        // Vehicle info
        const vehicles = await Promise.all(posts.map(async (post) => {
            const vehicleInfo = await getDatavehicle(post.Id_Vehiculo);
            return vehicleInfo;
        }));
        const fixedVehicles = vehicles.map(vehiclesArray => vehiclesArray[0]);

        // Obtener información de archivos para cada publicación
        const postsWithFiles = await Promise.all(posts.map(async (post) => {
            const folderPath = path.join(__dirname, `../../uploads/${post.Carpeta_Documentos}/reparacion${post.Fecha_Publicacion.toLowerCase()}`);
            const files = await getFilesInFolder(folderPath);
            return { ...post.toObject(), files };
        }));

        // Obtener los nombres de los usuarios que han comentado
        const users = await Promise.all(posts.map(async (post) => {
            if (Array.isArray(post.Comentarios) && post.Comentarios.length > 0) {
                const userNames = await Promise.all(post.Comentarios.map(async (comment) => {
                    if (comment.Id_Usuario) {
                        return await getNameUser(comment.Id_Usuario);
                    }
                    return null;
                }));
                return userNames;
            }
            return null;
        }));

        return res.status(200).json({
            ok: true,
            posts: postsWithFiles,
            admins: fixedAdmins,
            vehicles: fixedVehicles,
            users: users,
            currentPage: page,
            totalPages: Math.ceil(totalPosts / perPage)
        });

    } catch (err) {
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
}

exports.searchByPlates = async (req, res) => {
    try {
        const placas = req.params.value;
        const page = 1;
        const perPage = 10;

        const startIndex = (page - 1) * perPage;
        const endIndex = page * perPage;

        const totalPosts = await post_model.countDocuments({ Estatus: 'Activa', Carpeta_Documentos : { $regex: new RegExp(placas, 'i') } });

        const posts = await post_model.find({ Estatus: 'Activa', Carpeta_Documentos : { $regex: new RegExp(placas, 'i') } })
            .skip(startIndex)
            .limit(perPage);

        if (!posts || posts.length === 0) {
            return res.status(404).json({
                ok: false,
                message: 'No hay publicaciones'
            });
        }

        // Admin info 
        const admins = await Promise.all(posts.map(async (post) => {
            const adminInfo = await getDataAdmin(post.Id_Administrador);
            return adminInfo;
        }));
        const fixedAdmins = admins.map(adminArray => adminArray[0]);

        // Vehicle info
        const vehicles = await Promise.all(posts.map(async (post) => {
            const vehicleInfo = await getDatavehicle(post.Id_Vehiculo);
            return vehicleInfo;
        }));
        const fixedVehicles = vehicles.map(vehiclesArray => vehiclesArray[0]);

        // Obtener información de archivos para cada publicación
        const postsWithFiles = await Promise.all(posts.map(async (post) => {
            const folderPath = path.join(__dirname, `../../uploads/${post.Carpeta_Documentos}/reparacion${post.Fecha_Publicacion.toLowerCase()}`);
            const files = await getFilesInFolder(folderPath);
            return { ...post.toObject(), files };
        }));

        // Obtener los nombres de los usuarios que han comentado
        const users = await Promise.all(posts.map(async (post) => {
            if (Array.isArray(post.Comentarios) && post.Comentarios.length > 0) {
                const userNames = await Promise.all(post.Comentarios.map(async (comment) => {
                    if (comment.Id_Usuario) {
                        return await getNameUser(comment.Id_Usuario);
                    }
                    return null;
                }));
                return userNames;
            }
            return null;
        }));

        return res.status(200).json({
            ok: true,
            posts: postsWithFiles,
            admins: fixedAdmins,
            vehicles: fixedVehicles,
            users: users,
            currentPage: page,
            totalPages: Math.ceil(totalPosts / perPage)
        });

    } catch (err) {
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
}

exports.addComment = async (req, res) => {
    const body = req.body;
    
    try{
        const id = req.params.id;
        const { Fecha, Hora } = await getDate2();
        const user_type = await getUserType(body.Id_Usuario);

        const post = await post_model.findByIdAndUpdate(
            id, 
            {
                $push : {
                    Comentarios : {
                        Id_Usuario : body.Id_Usuario,
                        Tipo_Usuario : user_type,
                        Contenido : body.Contenido,
                        Hora : Hora,
                        Fecha : Fecha
                    }
                }
            },
            { new : true }
        );

        if(!post){
            return res.status(404).json({
                ok : false,
                message : 'Publicacion no encontrada'
            });
        }

        return res.status(200).json({
            ok: true,
            message: 'Comentario enviado',
            post : post
        });

    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}

exports.deletePost = async(req, res) => {
    const id = req.params.id;
    
    try{
        await post_model.findByIdAndDelete(id);

        return res.status(200).json({
            ok : true,
            message : 'Publicacion eliminada'
        });
    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}

exports.deletePostRepair = async(req, res) => {
    const id = req.params.id;
    
    try{
        await post_model.findOneAndDelete({Id_Reparacion : id});

        return res.status(200).json({
            ok : true,
            message : 'Publicacion eliminada'
        });
    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}

exports.countPosts = async (req, res) => {
    try {
        const result = await post_model.aggregate([
            { $match : { Estatus : 'Activa' } },
            { $group: { _id: '$Departamento', count: { $sum: 1 }} },
            { $sort: { count: -1 } },
        ]);


        return res.status(200).json({
            ok: true,
            departamentos: result
        });
    } catch (err) {
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
};

//Estadisticas de las reparaciones
exports.statistics = async (req, res) => {
    try {
        const activas = await post_model.aggregate([
            { $match : { Estatus : 'Activa' } },
            { $group: { _id: '$Departamento', count: { $sum: 1 }} },
            { $sort: { count: -1 } },
        ]);

        return res.status(200).json({
            ok: true,
            pendientes: activas,
        });
    } catch (err) {
        return res.status(500).json({
            ok: false,
            message: 'Error interno del servidor'
        });
    }
};

async function getFilesInFolder(folderPath) {
    try {
        const files = await fs.readdir(folderPath);
        return files;
    } catch (error) {
        console.error('Error al obtener archivos en la carpeta:', error.message);
        return [];
    }
}

async function getDataAdmin(id){
    const projection = {
        Nombre : 1,
        Email : 1,
        RPE : 1,
        Celular : 1,
        Fecha_Registro : 1,
        Hora_Registro : 1
    };

    try{
        const admins = await admin_model.find({_id : id}, projection);

        return admins;
    }catch(err){
        throw new Error('Administrado no encontrado');
    }
}

async function getDatavehicle(id){
    try{
        const vehicles = await vehicle_model.find({_id : id});

        if(!vehicles){
            console.log('No hay vehiculos ');
        }

        return vehicles;
    }catch(err){    
        throw new Error('Error al obtener la informacion del vehiculo');
    }
}

async function getUserType(id){
    try {
        const admin = await admin_model.findOne(
            { _id: id },
            { Cuenta : 1 }
        ).lean();

        if (admin) {
            console.log('Cuenta:', admin.Cuenta);
            return 'Administradores';
        }

        const workshop = await workshop_model.findOne(
            { _id: id },
            { Cuenta : 1 }
        ).lean();

        if (workshop) {
            console.log('Cuenta:', workshop.Cuenta);
            return 'Talleres';
        }
  
    } catch (err) {
        console.error('Error al buscar el usuario:', err);
        throw new Error('Error al buscar el usuario');
    }
}

async function getNameUser(id){
    try {
        const admin = await admin_model.findById(
            id,
            { Nombre : 1 }
        ).lean();

        if (admin) {
            return admin.Nombre;
        }

        const workshop = await workshop_model.findById(
            id,
            { Nombre : 1 }
        ).lean();

        if (workshop) {
            return workshop.Nombre;
        }
    } catch (err) {
        console.error('Error al buscar el usuario:', err);
        throw new Error('Error al buscar el usuario');
    }
}