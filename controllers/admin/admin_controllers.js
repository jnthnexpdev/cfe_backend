const admin_model = require('../../models/admin/admin_model');
const { getDate } = require('../../utils/date');

exports.createAccount = async(req, res) => {
    const {Fecha, Hora} = await getDate();
    const body = req.body;

    try{
        const check_email = await admin_model.findOne({Email: body.Email});

        if(check_email){
            return res.status(401).json({
                ok : false,
                message : 'Este correo ya esta registrado.'
            });
        }

        const new_admin = new admin_model({
            Nombre : body.Nombre,
            Email : body.Email,
            Password : body.Password,
            RPE: body.RPE,
            Departamento : body.Departamento,
            Fecha_Registro : Fecha,
            Hora_Registro : Hora,
            Cuenta : 'Administrador',
            Celular : body.Celular,
            Notificationes : [] 
        });

        await new_admin.save();

        return res.status(200).json({
            ok : true,
            message : 'Cuenta creada',
            user : new_admin
        });
        
    }catch(err){
        res.status(500).json({
            ok : false,
            message : 'Error al crear cuenta.'
        });
    }
}

exports.searchAdmin = async(req, res) => {
    const id = req.params.id;

    try{

        const admin = await getDataAdmin(id);

        if(!admin){
            return res.status(404).json({
                ok : false,
                message : 'Administrador no encontrado'
            });
        }

        return res.status(200).json({
            ok : true,
            admin : admin
        });

    }catch(err){
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}