const manager_model = require('../../models/manager/manager_model');
const { getDate } = require('../../utils/date');

exports.createAccountManager = async(req, res) => {
    const {Fecha, Hora} = await getDate();
    const body = req.body;

    try{
        const check_email = await manager_model.findOne({Email: body.Email});

        if(check_email){
            return res.status(401).json({
                ok : false,
                message : 'Este correo ya esta registrado.'
            });
        }

        const new_manager = new manager_model({
            Nombre : body.Nombre,
            Email : body.Email,
            Password : body.Password,
            RPE: body.RPE,
            Departamento : body.Departamento,
            Fecha_Registro : Fecha,
            Hora_Registro : Hora,
            Cuenta : 'Jefe',
            Celular : body.Celular,
            Notificationes : [] 
        });

        await new_manager.save();

        return res.status(200).json({
            ok : true,
            message : 'Cuenta creada',
            user : new_manager
        });
        
    }catch(err){
        console.log(err)
        res.status(500).json({
            ok : false,
            message : 'Error al crear cuenta.'
        });
    }
}

exports.searchManager = async(req, res) => {
    const id = req.params.id;

    try{

        const manager = await manager_model.findById(id);

        if(!manager){
            return res.status(404).json({
                ok : false,
                message : 'Jefe no encontrado'
            });
        }

        return res.status(200).json({
            ok : true,
            manager : manager
        });

    }catch(err){
        console.log(err)
        return res.status(500).json({
            ok : false,
            message : 'Error interno del servidor'
        });
    }
}