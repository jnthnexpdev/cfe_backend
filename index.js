const express = require('express');
const cors = require('cors');
const body_parser = require('body-parser');
const { connect } = require('./db/connection');
const dotenv = require('dotenv');
require('dotenv').config();

const app = express();

app.use(cors({ origin : 'http://localhost:4200', credentials: true }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:4200");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATH, DELETE");
    next();
}); 

// Configura la ruta para servir archivos estáticos desde la carpeta de imágenes

app.use(body_parser.json({limit : '5mb'}));
app.use(body_parser.urlencoded({limit: '5mb', extended : true}));
app.use(express.json());

//mongodb
connect();

//routes
app.use(require('./routes/index_routes'));

app.listen(process.env.PORT, () => {
    console.log('Server port: ', process.env.PORT);
});