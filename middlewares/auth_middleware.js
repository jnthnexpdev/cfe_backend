const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const token_key = process.env.SECRET_KEY;
dotenv.config();

function user_auth(){

    const allowedUserTypes = ['Administrador', 'Taller', 'Jefe'];
    
    return function(req, res, next){
        const token = req.signedCookies.token;

        if(!token){
            return res.status(401).json({
                ok : false,
                message : 'No se ha proporcionado el token'
            });
        }

        jwt.verify(token, token_key, (err, decoded) => {
            if(err){
                return res.status(401).json({
                    ok : false,
                    message : 'Token invalido'
                });
            }

            if (!decoded || !allowedUserTypes.includes(decoded.userType)) {
                return res.status(403).json({
                    ok: false,
                    message: 'Acceso no autorizado'
                });
            }

            next();
        });
    }

}

module.exports = {user_auth};