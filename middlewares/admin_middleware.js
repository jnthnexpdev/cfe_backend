const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const token_key = process.env.SECRET_KEY;
dotenv.config();

function admin_auth(){
    return function(req, res, next){
        const token = req.signedCookies.token;

        if(!token){
            return res.status(401).json({
                ok : false,
                message : 'No se ha proporcionado el token'
            });
        }

        jwt.verify(token, token_key, (err, decoded) => {
            if(err){
                return res.status(401).json({
                    ok : false,
                    message : 'Token invalido'
                });
            }

            if(!decoded || decoded.userType !== 'Administrador'){
                return res.status(403).json({
                    ok : 'false',
                    message : 'Acceso no autorizado'
                });
            }

            next();
        });
    }

}

module.exports = {admin_auth};