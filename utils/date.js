const axios = require('axios');
const url_time_api = 'http://worldtimeapi.org/api/timezone/America/Mexico_City';

async function getDate(){
    try{
        const response = await axios.get(url_time_api);

        if(response.status === 200){
            const data = response.data;
            const dateTimeString = data.datetime;
            const dateTime = new Date(dateTimeString);
            const year = dateTime.getFullYear();
            const month = dateTime.getMonth() + 1;
            const day = dateTime.getDate();
            const hours = dateTime.getHours() > 12 ? dateTime.getHours() - 12 : dateTime.getHours();
            const minutes = dateTime.getMinutes() < 10 ? '0' + dateTime.getMinutes() : dateTime.getMinutes();
            const ampm = dateTime.getHours() >= 12 ? 'PM' : 'AM';

            const Fecha = `${day}/${month}/${year}`;
            const Hora = `${hours}:${minutes} ${ampm}`;

            return { Fecha, Hora };
        }
    }catch(error){
        console.error('Error al obtener la fecha.');
        throw error;
    }
}

async function getDate2() {
    try {
        const response = await axios.get(url_time_api);

        if (response.status === 200) {
            const data = response.data;
            const dateTimeString = data.datetime;
            const dateTime = new Date(dateTimeString);
            const year = dateTime.getFullYear();
            const month = dateTime.getMonth() + 1;
            const day = dateTime.getDate();
            let hours = dateTime.getHours() > 12 ? dateTime.getHours() - 12 : dateTime.getHours();
            const minutes = dateTime.getMinutes() < 10 ? '0' + dateTime.getMinutes() : dateTime.getMinutes();
            const ampm = dateTime.getHours() >= 12 ? 'PM' : 'AM';

            // Agregar un "0" a la izquierda si la hora es menor que 10
            hours = hours < 10 ? '0' + hours : hours;

            const Fecha = `${day}-${month}-${year}`;
            const Hora = `${hours}:${minutes} ${ampm}`;

            return { Fecha, Hora };
        }
    } catch (error) {
        console.error('Error al obtener la fecha.');
        throw error;
    }
}


module.exports = {getDate, getDate2};